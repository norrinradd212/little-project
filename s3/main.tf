resource "aws_s3_bucket" "s3" {
  bucket = "terra-bucket"
  acl    = "private"
  region = var.region

  tags = {
    Name        = "terra-bucket"
    Environment = "Dev"
  }
}