# ---------------Setting for remote_state---------------------
terraform {
  backend "s3" {
    bucket = "terra-bucket-test-for-oleg"
    region = "eu-central-1"
    key = "test/terraform.tfstate"
    }
}
provider "aws" {
  region = var.region
}