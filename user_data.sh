#!/bin/bash

apt update && apt upgrade
apt install python3 python3-pip -y
pip3 install flask


myip=`curl http://169.254.169.254/latest/meta-data/public-ipv4`

cat <<EOF > /home/ubuntu/start_scr.py
from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello, I use Flask and this is my ip $myip!'

app.run(host='0.0.0.0', port=80)
EOF

python3 /home/ubuntu/start_scr.py